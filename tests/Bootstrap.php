<?php

// Define config vars
define("DEBUG", true);

define("DB_ENABLED",true);
define("DB_TYPE", $GLOBALS['DB_TYPE']);
define("DB_HOST", $GLOBALS['DB_HOST']);
define("DB_PORT", $GLOBALS['DB_PORT']);
define("DB_USERNAME", $GLOBALS['DB_USERNAME']);
define("DB_PASSWORD", $GLOBALS['DB_PASSWORD']);
define("DB_NAME", $GLOBALS['DB_NAME']);