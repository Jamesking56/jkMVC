<?php

/*
	Database Test File
	Tests the Database Class to see if it is working correctly.
 */

require_once("src/libs/Database.php");

class DatabaseTest extends PHPUnit_Framework_TestCase
{
	private $database;

	/**
	 * Sets up the test
	 */
	public function setUp()
	{
		$this->database = new Database();
	}

	/**
	 * Clears the tests when complete
	 */
	public function tearDown()
	{
		$this->database = null;
	}

	public function testIfDatabaseIsConnected()
	{
		$this->assertTrue($this->database->isConnected());
	}
}

?>