<?php

require_once("src/libs/ErrorHandler.php");
/**
* ErrorHandler test file - Tests the ErrorHandler
*/
class ErrorHandlerTest extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		ErrorHandler::$errorController = false;
		ErrorHandler::$debug = DEBUG;
	}

	public function testIfErrorHandlerWorks()
	{
		$this->assertTrue(ErrorHandler::handlePHPError(E_USER_ERROR,"Hello World.","ErrorHandlerTest.php",17, true));
		$this->assertTrue(ErrorHandler::handleHTTPError(404, true));
	}
}