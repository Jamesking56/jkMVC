# Welcome to the jkMVC Project [![Project Status](http://stillmaintained.com/Jamesking56/jkMVC.png)](http://stillmaintained.com/Jamesking56/jkMVC) [![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/Jamesking56/jkmvc/trend.png)](https://bitdeli.com/free "Bitdeli Badge")
jkMVC is my own private MVC that I will be using as a base for all of my future projects.

At the moment, I use a mixture of procedural and object oriented programming so I've decided to move to MVC so that I can start following the DRY principle.

I've made this open source for the world so that you can use it too and can contribute to making it better and reporting any problems you face with it.

## Server Requirements
To run this system your server has to have PHP 5.3.0+ and PDO extension.

## How do I download it?
You can hit Tags on the right hand side of your screen to see all of the *versioned* copies of the system (The ones with a version number assigned) or use the *ZIP* button at the top left to get the latest *In Development* version (**Not recommended**).

Before you download the *In Development* version, check to see if the below image says *Build Successful*. If it doesn't, then the *In Development* version has problems at the moment and you should try again later (See *Automated Testing* below):

[![Build Status](https://travis-ci.org/Jamesking56/jkMVC.png?branch=master)](https://travis-ci.org/Jamesking56/jkMVC)

## How do I use it?
Soon, the [Wiki](https://github.com/Jamesking56/jkMVC/wiki) will have some guides on how to set it up and start using it for your own benefits. I will write some guides when I've got v0.1a released for you.

## Is this system free?
This system is completely free for you to use. I do pose some licensing restrictions (see ***Licensing*** below) but those are for copying, modifying and re-releasing online. For usage in your own projects it is completely free.

## I've found a bug, can I report it?
Sure! Please report any issues you find over in the [Issues](https://github.com/Jamesking56/jkMVC/issues) tab attached to this repository. Reporting bugs really helps me and the community using this system.

## I'm a developer wishing to contribute to this project. How can I do that?
Contributing is easy if you're familiar with how GitHub works! Just hit *Fork* above to create your own copy, edit your new copy and send a [Pull Request](https://github.com/Jamesking56/jkMVC/pulls/new) to send your changes back to me. I'll then have your changes tested and check them over before adding them to this repository.

## I have another question
[Create a new Issue](https://github.com/Jamesking56/jkMVC/issues/new) and I'll answer it for you.

----
# Automated Testing
Every change to this repository (by me or via a *Pull Request*) is automatically tested using PHPUnit and Travis CI.

Travis will test in these environments using pre-written tests found in the tests folder:
* PHP 5.3 with MySQL
* PHP 5.3 with PostgreSQL
* PHP 5.4 with MySQL
* PHP 5.4 with PostgreSQL

To check whether the current *In Development* version is passing the tests, see if the image below says *Build Passing*:

[![Build Status](https://travis-ci.org/Jamesking56/jkMVC.png?branch=master)](https://travis-ci.org/Jamesking56/jkMVC)

*Pull Requests* sent to me via the Issues tab will have Travis status underneath them when you click on them, it'll show up green if the tests are still passing. I will only accept *Pull Requests* that pass the tests so that we don't create more bugs or problems.

----
# Licensing
<p align="center"><a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.</p>
