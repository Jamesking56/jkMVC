<?php

/**
* Example Model
*/
class ExampleModel extends Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function blah()
	{
		return 10 + 10;
	}

	public function getNames()
	{
		$stmt = $this->db->prepare("SELECT * FROM test");
		$stmt->execute();

		return $stmt->fetchAll();
	}
}