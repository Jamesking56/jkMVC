<?php

/**
* Error Page Controller - Runs when the controller for the page is not found
*/
class Error extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->notFound();
	}

	public function notFound()
	{
		$this->view->render('error/index');
	}

	public function serverError()
	{
		$this->view->render('error/server-error');
	}

	public function forbidden()
	{
		$this->view->render('error/forbidden');
	}
}

