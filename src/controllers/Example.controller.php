<?php

/**
* Example Page Controller
*/
class Example extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->view->render('example/index');
	}

	public function other($arg = false)
	{
		$this->view->render('example/other');
	}
}

