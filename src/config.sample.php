<?php

/*
	RENAME THIS TO config.php WHEN FINISHED!
 */

// BASE URL - Must have a trailing '/'
// DON'T FORGET TO CHANGE THIS
define('URL', 'http://www.example.com/');

// Database Details
// Is your app using a Database?
define('DB_ENABLED', false);

// Database Type (Uses PDO plugin for PHP. So any supported by those will work.)
define('DB_TYPE', "mysql");

// Database Server
define('DB_HOST', "localhost");

// Database Port (3306 by default)
define('DB_PORT', 3306);

// Database Username & Password
define('DB_USERNAME', "root");
define('DB_PASSWORD', "");

// Database Name to use
define('DB_NAME', "myDatabase");

// Debug Mode - Show all errors (useful for development)
define('DEBUG', true);