<?php

/**
* 
*/
class Bootstrap
{
	
	function __construct()
	{
		// Start Error Handler
		$this->setupErrorHandler();

		$url = (isset($_GET['url'])) ? $_GET['url'] : null;
		$url = rtrim($url, "/");
		$url = explode("/", $url);

		if (empty($url[0])) {
			require 'controllers/Index.controller.php';
			$controller = new Index();
			$controller->index();
			return false;
		}

		if($url[0] == "error")
		{
			if(isset($url[1]))
			{
				ErrorHandler::handleHTTPError($url[1]);
				return false;
			}
			else
			{
				ErrorHandler::handleHTTPError(404);
				return false;
			}
		}

		$file = 'controllers/'.$url[0].'.controller.php';

		if(file_exists($file))
		{
			require $file;
			$controller = new $url[0];
			$controller->loadModel($url[0]);
		}
		else
		{
			ErrorHandler::handleHTTPError(404);
			return false;
		}

		// Calling Methods
		if(isset($url[2]))
		{
			if(method_exists($controller, $url[1]))
			{
				$controller->{$url[1]}($url[2]);
			}
			else
			{
				ErrorHandler::handleHTTPError(404);
				return false;
			}
		}
		else
		{
			if(isset($url[1]))
			{
				if(method_exists($controller, $url[1]))
				{
					$controller->{$url[1]}();
				}
				else
				{
					require 'controllers/Error.controller.php';
					$controller = new Error();
					$controller->loadModel('Error');
					$controller->index();
					return false;
				}
			}
			else
			{
				$controller->index();
			}
		}
	}

	public function setupErrorHandler()
	{
		require 'controllers/Error.controller.php';
		require 'libs/ErrorHandler.php';
		$controller = new Error();
		ErrorHandler::$errorController = $controller;
		ErrorHandler::$debug = DEBUG;
		set_error_handler('ErrorHandler::handlePHPError');
	}
}