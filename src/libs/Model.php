<?php

/**
* Base Model - All Models are children of this.
*/
class Model
{
	function __construct()
	{
		if(DB_ENABLED)
		{
			$this->db = new Database();
		}
	}
}