<?php



/**
* Error Handler - Catches all PHP errors and manages all HTTP errors (404, 403 etc.)
* This class is STATIC and can only have one instatiation (instanciated in Bootstrap.php)
*/
class ErrorHandler
{
	public static $errorController;
	public static $debug = false;

	public static function handlePHPError($errorType, $errorString, $errorFile, $errorLine, $testMode=false)
	{
		// PHP Error, Warning or Notice has occured!
		switch($errorType)
		{
			case E_ERROR:
			case E_USER_ERROR:
			case E_RECOVERABLE_ERROR:
			default:
				if($testMode)
					return true;

				if(self::$debug)
				{
					echo "<pre><b>FATAL</b> [T: $errorType] [F: $errorFile] [L: $errorLine]<br />$errorString<br /></pre>";
					exit;
				}
				else
				{
					echo "A fatal error occurred, please contact site administrator!";
					exit;
				}
				break;

			case E_WARNING:
			case E_USER_WARNING:

				if(self::$debug)
				{
					echo "<pre><b>ERROR</b> [T: $errorType] [F: $errorFile] [L: $errorLine]<br />$errorString<br /></pre>";
				}
				else
				{
					echo "An error has occurred!";
				}
				break;


			case E_NOTICE:
			case E_USER_NOTICE:
				if(self::$debug)
				{
					echo "<pre><b>WARNING</b> [T: $errorType] [F: $errorFile] [L: $errorLine]<br />$errorString<br /></pre>";
				}
				else
				{
					echo "A warning has occurred!";
				}
				break;
		}
	}

	public static function handleHTTPError($httpCode, $testMode=false)
	{
		switch ($httpCode) {
			case 400:
			case 502:
			case 503:
			default:
				// Server Error
				if(isset(self::$errorController))
				{
					self::$errorController->serverError();
				}
				break;

			case 401:
			case 403:
				// Forbidden
				if(isset(self::$errorController))
				{
					self::$errorController->forbidden();
				}
				break;

			case 404:
			case 410:
				// Not Found
				if($testMode)
					return true;

				if(isset(self::$errorController))
				{
					self::$errorController->notFound();
				}
				break;
		}
	}
}