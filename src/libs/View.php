<?php

/**
* Base View - All Views are children of this.
*/
class View
{
	
	function __construct()
	{
		// nothing
	}

	public function render($name)
	{
		require 'views/header.php';
		require 'views/'.$name.'.php';
		require 'views/footer.php';
	}
}