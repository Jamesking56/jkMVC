<?php

/**
* Database Class
*/
class Database extends PDO
{	
	private $connected = false;

	function __construct()
	{
		if(DB_ENABLED)
		{
			if($this->isAvailable(DB_TYPE))
			{
				$dsn = DB_TYPE . ":host=" . DB_HOST . ";port=".DB_PORT.";dbname=".DB_NAME;
				$connect = parent::__construct($dsn, DB_USERNAME, DB_PASSWORD);
				$this->connected = true;
			}
			else
			{
				throw new PDOException("Database Type ".DB_TYPE." is not available on your server. Please contact your web host.");
			}
		}
		else
		{
			throw new PDOException("Database connectivity is disabled. Please edit config.php to enable it.");
		}
	}

	public function isAvailable($dbType)
	{
		$drivers = parent::getAvailableDrivers();

		foreach($drivers as $driver)
		{
			if($driver == $dbType)
			{
				return true;
			}
		}
		return false;
	}

	public function isConnected()
	{
		return $this->connected;
	}
}