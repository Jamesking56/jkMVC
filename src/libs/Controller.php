<?php

/**
* Base Controller - All Controllers are children of this.
*/
class Controller
{
	function __construct()
	{
		$this->view = new View();
	}

	public function loadModel($name)
	{
		$path = 'models/'.$name.'.model.php';

		if(file_exists($path))
		{
			require $path;
			$modelName = $name . "Model";
			$this->model = new $modelName;
		}
	}
}